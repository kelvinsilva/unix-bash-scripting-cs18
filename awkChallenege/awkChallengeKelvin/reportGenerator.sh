#!/usr/bin/bash  

awk 'BEGIN {printf "%-15s%-8s%-5s\n-------      --------  -----\n", "Product", "Quantity", "Price"} 

{printf ("%-15s%-8s%-5s\n",  $1, $2, $3)}' $1 
