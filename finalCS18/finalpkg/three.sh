#/bin/bash

wget https://www.gutenberg.org/files/132/132.txt
cat 132.txt | sed 's/[^a-zA-Z ]//g' > testFile.txt

awk '
{
	for (i = 1; i <= NF; i++)
		freq[$i]++
}
END {
	for (word in freq)
		printf "%d\t%s\n",  freq[word], word
}' testFile.txt > list.txt

sort -n -k1,1 list.txt > sortedTemp.txt

echo "Least 15 words in Art of war: " > report.txt
head -15 sortedTemp.txt >> report.txt

echo "Top 15 words in Art of war: " >> report.txt
tail -15 sortedTemp.txt >> report.txt

cat sortedTemp.txt | sed 's/[^a-zA-Z]//g' > temp2.txt
echo "Length of Longest word: " >> report.txt
awk ' { 
                        if ( length > x ) { 
                           
                            x = length; 
                            y = $0 
                        } 
                    }END{ print x }' temp2.txt >> report.txt
                    
echo "shortest word: " >> report.txt
awk '(NR==1||length<short){
        short=length
    
    } END {print short}' temp2.txt  >> report.txt
    
echo "average length of word: " >> report.txt
numchars=$(wc -m testFile.txt | sed 's/[^0-9]//g')
numwords=$(wc -w testFile.txt | sed 's/[^0-9]//g')
avg=$((numchars / numwords))
echo $avg >> report.txt

echo "number of sentences: " >> report.txt
grep -o -c '[!?.]' 132.txt >> report.txt

rm 132.txt
rm list.txt
rm sortedTemp.txt
rm temp2.txt
rm testFile.txt
