#!/bin/bash

if [ "$#" -lt 3 ]; then

     echo "you have not entered the minimum expected number  of parameters"
elif [ "$#" -gt 3 ]; then

     echo "you have exceeded the expected number of parameters"
else 

     echo "Looks good"
fi 
