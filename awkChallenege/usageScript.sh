#argument c: only count letters
#argument s: set print output flag
#argument i: set ignore case flag
#argument l: set filnemaes only flag
#argument e: assign certain pattert to script
#no argument: call usage function
BEGIN {
    while ((c = getopt(ARGC, ARGV, "ce:svil")) != -1) {
        if (c == "c")
            count_only++
        else if (c == "s")
            no_print++
        else if (c == "v")
            invert++
        else if (c == "i")
            IGNORECASE = 1
        else if (c == "l")
            filenames_only++
        else if (c == "e")
            pattern = Optarg
        else
            usage()
    }
