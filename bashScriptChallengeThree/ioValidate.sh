#!/bin/bash
DIRPLACE=/tmp
INFILE=/home/kelvin/input.data
OUTFILE=/home/kelvin/output.results

outputFileWantedDirectory=$(dirname $OUTFILE)

#parse infile and outfile to obtain the filename with extension. takes out path
fileNameInput=$(echo "${INFILE##*/}")
fileNameOutput=$(echo "${OUTFILE##*.}")


#lists all contents in the directory that contains the input and output data, then pipes to cat and pipes to grep to search for file name. if grep
#finds something, then it will fille the variable fileToReadEx with a line from ls -la which makes the variable size greater than zero,
# if it doesnt find anything fileToReadEx is empty, and its size is zero

fileToReadEx=$(ls -la $outputFileWantedDirectory | cat | grep -iw $fileNameInput) 

#if the fileToReadEx variable contains something inside (size > 0), then grep returned a successful search, which means file name exists, if file not found, exit automatically
if [ ${#fileToReadEx} -eq 0 ]; then

     echo "File to read not found"
     exit
fi

#if file to read exists, now check output file....
#search for outputfile name and insert into variable "outputFileWantedEx"
#if grep find something, outputFileWantedEx will contain a size > 0, which means that the file exists in the directory.
#if grep did not find something then outputFileWantedEx will contain a size < 0, which means that the file does not exist in directory

outputFileWantedEx=$(ls -la $outputFileWantedDirectory | cat | grep -iw $fileNameOutput)

#once we get the file name successfully, remember grep returns to us a line that ls -la gave us, which includes read and write permission in the line.
#so i get head of a starting few number of bytes from what ls -la gave us which is the permissions (-rwrx-rx-wrw stuff)

permissionList=$(echo $outputFileWantedEx | head -c 11)

#after getting the -rwr-wr-rw-r-r part of the string, we search that for "w" which means write. if there is a w in the string that means we can write to file
#if grep finds it, then the variable is greater than 0, and if it does not find it, it means varible is empty

testWrite=$(echo $permissionList | grep w)

#if variable is empty, we dont have write permission, if variable is greater than zero we have write permission
if [ ${#testWrite} -eq 0 ]; then

     echo "File to write has no write permission or does not exist"
     exit
fi

#if program reaches here, it means it did not exit, which means that the if statements fell through, which means that all variables contained size greater than zero
#which means that grep found file to read, and in output permissions it also found a "w", which means file to read exists and file to write has write permission
#print success statement:

echo "Read file exists and output file has write permission"

