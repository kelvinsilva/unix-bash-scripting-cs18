
/*
Kelvin silva

each item has an id, price per unit, and quantity, and a deal associated with it.
if we want to get a deal, we can set an arbitrary price for an arbitrary number of items in the superMarketPricing.itemsDeals.
that way we can configure any deal that we want like buy one get one free, or buy 4 and get the 5th 50% off and the 6th for free.
*/
create schema supermarketpricing;

create table supermarketpricing.itemsdeals (
    
    DEAL_ID INT UNIQUE,
    DEAL_PRICE MONEY NOT NULL,
    QUANTITY_PER_DEAL INT NOT NULL,
    PRIMARY KEY(DEAL_ID)
);

create table supermarketpricing.orderhistory(

    ORDER_ID INT UNIQUE,
    DATE_OF_ORDER TIMESTAMP NOT NULL,
    QUANTITY_SOLD INT NOT NULL,
    PRICE_PAID INT NOT NULL
);

create table supermarketpricing.items (

    ITEMID INT      UNIQUE,
    NAME VARCHAR(100) NOT NULL,
    PRICE_PER_UNIT  MONEY NOT NULL,
    QUANTITY        INT NOT NULL,
    
    DEAL_ID INT references supermarketpricing.itemsdeals(DEAL_ID),
    PRIMARY KEY(ITEMID)
);

